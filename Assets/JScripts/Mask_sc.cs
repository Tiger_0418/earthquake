﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Mask_sc : MonoBehaviour {

	public bool start;

	public GameObject 三級地震體驗;
	public GameObject 四級地震體驗;
	public GameObject 五級地震體驗;
	public GameObject 六級地震體驗;
	public GameObject 七級地震體驗;
	public float time;

//	public Text ti;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//ti.text = time.ToString();


		if (start) {
			time = time + 1 * Time.deltaTime;


			if (time < 61f && time > 55f) {
				三級地震體驗.SetActive (true);
			} else {
				三級地震體驗.SetActive (false);
			}

			if (time < 71f && time > 62f) {
				四級地震體驗.SetActive (true);
			} else {
				四級地震體驗.SetActive (false);
			}

			if (time < 89f && time > 76f) {
				五級地震體驗.SetActive (true);
			} else {
				五級地震體驗.SetActive (false);
			}

			if (time < 107f && time > 93f) {
				六級地震體驗.SetActive (true);
			} else {
				六級地震體驗.SetActive (false);
			}

			if (time < 125f && time > 111f) {
				七級地震體驗.SetActive (true);
			} else {
				七級地震體驗.SetActive (false);
			}
		} 
		else 
		{
			time = 0;
		}
	}
}
