﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sever : MonoBehaviour {

	public GameObject[] players;

	public Text players_num;
	public GameObject can;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		players = GameObject.FindGameObjectsWithTag ("Player");

		if (players.Length > 0)
		{
			players_num.text = players.Length.ToString();
		}
		if (players.Length == 0)
		{
			players_num.text = "0";
		}
	}
}
