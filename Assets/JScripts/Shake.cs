﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {

	public bool X;
	public float x_wave = 0.1f;
	public float x_speed = 50f;
	float x_shake = 0;

	public bool Y;
	public float y_wave = 0.1f;
	public float y_speed = 50f;
	float y_shake = 0;

	public bool Z;
	public float z_wave = 0.1f;
	public float z_speed = 50f;
	float z_shake = 0;

	public bool Random_value;

	public Vector3 x_pos;


	// Use this for initialization
	void Start () 
	{
		x_pos= this.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Random_value) 
		{
			x_shake = x_wave * (Mathf.Sin (Time.time * x_speed + Random.Range (-1, 1)));
			y_shake = y_wave * (Mathf.Sin (Time.time * y_speed + Random.Range (-1, 1)));
			z_shake = z_wave * (Mathf.Sin (Time.time * z_speed + Random.Range (-1, 1)));
		} 
		else 
		{
			x_shake = x_wave * (Mathf.Sin (Time.time * x_speed));
			y_shake = y_wave * (Mathf.Sin (Time.time * y_speed));
			z_shake = z_wave * (Mathf.Sin (Time.time * z_speed));
		}


		if (X)
		{
			this.transform.position = new Vector3 (this.transform.position.x+x_shake, this.transform.position.y, this.transform.position.z);
		}
		if (X == false)
		{
			this.transform.position = new Vector3 (x_pos.x,this.transform.position.y,this.transform.position.z);
		}
		if (Y)
		{
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y+y_shake, this.transform.position.z);
		}
		if (Y == false)
		{
			this.transform.position = new Vector3 (this.transform.position.x,x_pos.y,this.transform.position.z);
		}
		if (Z) 
		{
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z+z_shake);
		}
		if (Z == false)
		{
			this.transform.position = new Vector3 (this.transform.position.x,this.transform.position.y,x_pos.z);
		}
	}
}
