﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;
using UnityEngine.XR;

public class Player_sc : NetworkBehaviour {

	public GameObject video_gam = null;
    public bool bFirstTimeCheck = false;
	public bool click = false;

	public GameObject mask = null;
	public Mask_sc mask_s;

//	public const int COLLECT_COUNT = 10;
//	public float[] ty = new float[COLLECT_COUNT];
//	public int collectCompassCount = COLLECT_COUNT;

	void Start()
	{
		XRSettings.enabled = true;		
//        Input.location.Start();
//        Input.compass.enabled = true;
//		this.transform.Rotate(0, 90f, 0);
    }

    void Update ()
	{
        if(isServer)
        {
            if (Hardware.read == 65)
            {
                RpcCli(true);
                Hardware.read = 0;
            }
            if (Hardware.read == 66)
            {
                RpcCli(false);
                Hardware.read = 0;
            }
        }


		if (click && isClient) 
		{
			if (mask == null)
				mask = Instantiate (Resources.Load ("Camera")) as GameObject;
			mask_s = mask.GetComponent<Mask_sc> ();
			mask_s.start = true;

//			UpdateCollectCompass ();

//			if (collectCompassCount == -1) 
//			{
				if (!bFirstTimeCheck) 
				{
					Debug.Log ("Begin to play video...");
					
                	
//				float tY = ty [0]; // Input.compass.trueHeading;
//                if(tY < 303.0f)
//                    this.transform.Rotate(0, ((tY+360.0f) - 303.0f), 0);
//                else
//                    this.transform.Rotate(0, (tY  - 303.0f), 0);

					if (video_gam == null)
						video_gam = Instantiate (Resources.Load ("video")) as GameObject;
					video_gam.GetComponent<VideoPlayer> ().enabled = true;
					video_gam.GetComponent<VideoPlayer> ().loopPointReached += Player_sc_loopPointReached;
					bFirstTimeCheck = true;
				} 
				else 
				{
					if (!video_gam.GetComponent<VideoPlayer> ().isPlaying)
						video_gam.GetComponent<VideoPlayer> ().Play ();
				}
//			}
		} 
		if (!click && isClient) 
		{
            FreeResources();
			mask_s.start = false;
        }
	}

//	void UpdateCollectCompass() {
//		if (collectCompassCount > 0) {
//			
//			int index = collectCompassCount - 1;
//			ty[index] = Input.compass.trueHeading;
//
//			collectCompassCount--;
//
//			if (collectCompassCount <= 0) {
//				collectCompassCount = -1;
//
//				List<float> list = new List<float> ();
//				list.Sort (CompassValueHigher);
//
//				list.RemoveAt (0);
//				list.RemoveAt (0);
//
//				list.RemoveAt (list.Count - 1);
//				list.RemoveAt (list.Count - 1);
//
//				float totalY = 0f;
//				for (int i = 0; i < list.Count; ++i) {
//					totalY += list [i];
//				}
//				ty [0] = totalY / list.Count;
//			}
//		}
//	}

//	private int CompassValueHigher(float x, float y) {
//		if (x > y) {
//			return 1;
//		} else if (x < y) {
//			return -1;
//		}
//		return 0;
//	}
		
//	private bool once = true;
//	void OnApplicationFocus(bool hasFocus)
//	{
//		if (hasFocus && once)
//		{
//			float tY = Input.compass.trueHeading;
//			if(tY < 303.0f)
//				this.transform.Rotate(0, ((tY+360.0f) - 303.0f), 0);
//			else
//				this.transform.Rotate(0, (tY  - 303.0f), 0);

//			click = true;
//			bFirstTimeCheck = false;
//			collectCompassCount = COLLECT_COUNT;
//
//			once = false;
//		}
//		if (hasFocus == false)
//		{
//			once = true;
//		}
//	}

    private void FreeResources()
    {
        if (bFirstTimeCheck)
        {
            video_gam.GetComponent<VideoPlayer>().Stop();
        }
    }

    //To check videoplayer is reaching loopPoint
    private void Player_sc_loopPointReached(VideoPlayer source)
    {
        click = false;
    }

    [ClientRpc]
	public void RpcCli (bool x)
	{
		click = x;
	}
}
