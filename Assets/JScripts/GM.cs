using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class GM : MonoBehaviour
{
	//	[SyncVar]
//	public string sever_IP;
	public HostData[] data;

	public int sever_man;

	void Start()
	{
		#if UNITY_IOS || UNITY_ANDROID

		MasterServer.ClearHostList();//先清除原先的連線列表

		#else

		Network.InitializeServer(10, 8000, !Network.HavePublicAddress());
		MasterServer.RegisterHost("AAA", "BBB");

		#endif
	}

	void Update () 
	{
//		if (isServer)
//		{
//			sever_IP = Network.player.ipAddress;
//		}

		//判斷平台
		#if UNITY_IOS || UNITY_ANDROID

		MasterServer.RequestHostList("AAA");

		HostData[] data = MasterServer.PollHostList();
		if (data.Length > 0)//若是有搜尋到，則進行連線
		{
			Network.Connect(data[0].ip, data[0].port);
			sever_man = Network.connections.Length;
		}

		#else

		#endif
	}
}
