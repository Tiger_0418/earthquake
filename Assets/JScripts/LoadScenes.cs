﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.Networking;

public class LoadScenes : MonoBehaviour {

	public GameObject[] Videos;
	public VideoPlayer[] viplayer = new VideoPlayer[10];

	// Use this for initialization
	void Start () 
	{
		
	}



	// Update is called once per frame
	void Update () 
	{
		Videos = GameObject.FindGameObjectsWithTag ("Video");
	}
	public void Restart () 
	{
		//viplayer[0] = Videos[0].GetComponent<VideoPlayer>();
		//viplayer[0].enabled =! viplayer[0].enabled;

		for (int i = 0;i < Videos.Length;i++)
		{
			viplayer[i] = Videos[i].GetComponent<VideoPlayer>();
			viplayer[i].enabled =! viplayer[i].enabled;
		}
	}
}
