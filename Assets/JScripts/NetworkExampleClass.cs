﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkExampleClass : NetworkBehaviour
{
    public Color myColor;
    public Text text;

 
    public void Start()
    {
        if (isLocalPlayer)
        {
            myColor = Color.blue;
        }
        else
        {
            myColor = Color.red;
        }

        if (isServer)
        {
            Debug.Log("Object started on server");
        }
    }


    public void Update()
    {
        if (isServer)
        {
            if (Input.GetKey("a"))
            {
                RpcCallClientToAct(1);
            }

            if (Input.GetKey("s"))
            {
                RpcCallClientToAct(0);
            }
        }
    }

    // send command to clients to perform visual logic
    [ClientRpc]
    public void RpcCallClientToAct(int status)
    {        
        Debug.Log("i am client");
        ShowTextAct(status);
    }

    public void ShowTextAct(int status)
    {
        // TODO: create visual feedback for taking a potion
        text.text = string.Format("{0}", status);
    }

}
