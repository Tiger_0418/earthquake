﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SetAutoConnect : MonoBehaviour {
    public NetworkManager manager;
    public string server_ip = "192.168.1.150";
    public bool isClient = false;

    void Awake()
    {
        manager = GetComponent<NetworkManager>();
    }

    // Use this for initialization
    void Start () {
        if (isClient)
        {
            manager.networkPort = 7777;
            manager.networkAddress = server_ip;            
        }
    }

    private void onConnected(NetworkMessage netMsg)
    {
        Debug.Log("I am ready, Sir!");
    }

    // Update is called once per frame
    void Update () {
		if(isClient)
        {
            if (!NetworkClient.active && !NetworkServer.active && manager.matchMaker == null)
            {
               manager.StartClient();
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}
