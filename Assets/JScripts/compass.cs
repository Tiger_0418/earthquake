﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class compass : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		Input.location.Start ();
		Input.compass.enabled = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//transform.rotation = Quaternion.Euler(0, -Input.compass.magneticHeading, 0);
		transform.rotation = Quaternion.Euler(0, -Input.compass.trueHeading, 0);
	}
	void OnGUI() 
	{
		GUILayout.Label("Magnetometer reading: " + Input.compass.rawVector.ToString());
	}
}
